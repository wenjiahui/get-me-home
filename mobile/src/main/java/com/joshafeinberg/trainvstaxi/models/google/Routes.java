package com.joshafeinberg.trainvstaxi.models.google;

import android.text.SpannableString;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;

import java.util.List;

public class Routes {

    @Expose
    private List<Legs> legs;

    public List<Legs> getLegs() {
        return legs;
    }

    public Integer getRouteTime() {
        Integer routeTime;
        if (getLegs().size() == 0) {
            routeTime = -1;
        } else {
            Legs leg = getLegs().get(0);
            KeyValue duration = leg.getDuration();
            routeTime = duration.getValue();
        }

        return routeTime;
    }

    public String getStartAddress() {
        String startAddress;
        if (getLegs().size() == 0) {
            startAddress = "";
        } else {
            Legs leg = getLegs().get(0);
            startAddress = leg.getStartAddress();
        }

        return startAddress;
    }

    public CharSequence getDisplayString() {
        SpannableString empty = new SpannableString("");
        if (getLegs().size() == 0) {
            return empty;
        }

        Legs leg = getLegs().get(0);
        CharSequence charSequence = "";
        int stepCount = leg.getSteps().size();
        for (int i = 0; i < stepCount; ++i) {
            Step step = leg.getSteps().get(i);
            charSequence = TextUtils.concat(charSequence, step.getDisplayString());
            if (i+1 != stepCount) {
                charSequence = TextUtils.concat(charSequence, " ➡ ");
            }
        }
        return charSequence;
    }

    public Long getArrivalTime() {
        if (getLegs().size() == 0) {
            return 0L;
        }

        Legs leg = getLegs().get(0);
        if (leg.getSteps().size() == 0) {
            return 0L;
        }

        int location = 0;
        Step step = leg.getSteps().get(location);
        while (step.isWalking()) {
            if (leg.getSteps().size() <= ++location) {
                return 0L;
            }
            step = leg.getSteps().get(location);
        }
        return step.getArrivalTime();
    }
}
