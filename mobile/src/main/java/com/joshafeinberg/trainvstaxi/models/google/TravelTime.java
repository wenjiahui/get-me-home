package com.joshafeinberg.trainvstaxi.models.google;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;

import java.util.List;

public class TravelTime {

    private static final String OK = "OK";

    @Expose
    private String status;
    @Expose
    private List<Routes> routes;

    public List<Routes> getRoutes() {
        return routes;
    }

    public boolean isStatusOkay() {
        boolean isOkay;
        if (TextUtils.isEmpty(status)) {
            isOkay = false;
        } else {
            isOkay = status.equalsIgnoreCase(OK);
        }
        return isOkay;
    }
}
