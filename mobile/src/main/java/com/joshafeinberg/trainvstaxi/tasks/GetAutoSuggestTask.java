package com.joshafeinberg.trainvstaxi.tasks;

import android.os.AsyncTask;

import com.joshafeinberg.trainvstaxi.App;
import com.joshafeinberg.trainvstaxi.models.google.Predictions;
import com.joshafeinberg.trainvstaxi.network.GoogleRestClient;
import com.joshafeinberg.trainvstaxi.network.RestApiAdapter;

import java.util.LinkedHashMap;

public class GetAutoSuggestTask extends AsyncTask<String, Void, Predictions> {

    public static interface OnCompleteListener {
        public void onSuggestionsReturned(Predictions predictions);
    }

    private OnCompleteListener mOnCompleteListener;

    public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
        mOnCompleteListener = onCompleteListener;
    }

    @Override
    protected Predictions doInBackground(String... params) {
        String input = params[0];

        GoogleRestClient googleRestClient = RestApiAdapter.getInstance().getGoogleRestClient();

        LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
        options.put("key", App.GOOGLE_API_KEY);
        options.put("components", "country:us");
        options.put("input", input);


        return googleRestClient.getAutosuggestion(options);
    }

    @Override
    protected void onPostExecute(Predictions predictions) {
        mOnCompleteListener.onSuggestionsReturned(predictions);
    }
}
